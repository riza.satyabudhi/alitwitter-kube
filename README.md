# Ali Twitter, Containerization Approach

Twitter-like web app used Rails-Framemwork which can do:

1. Create a tweet
2. Delete a tweet

# Architecture
![Architecture](./architecture.png)

# Deployment

## Dependencies
1. minikube
2. kubectl
3. helm

## How to Install

### 1. Start kubernetes cluster using minikube
```
$ minikube start
```

### 2. Setup postgresql from bitnami using Helm
Supply custom values from `postgresqlchart/custom-values.yml`
```
$ helm repo add bitnami https://charts.bitnami.com/bitnami
$ helm install alitwitter-postgres -f postgresqlchart/custom-values.yml bitnami/postgresql
```

### 3. Setup app using Helm
```
$ helm install alitwitterchart alitwitterchart
```

### 6. Access App
Open from browser `<minikube_ip>:<node_port>/tweets`