# Dependencies
1. Minikube
2. Helm
3. Running postgresql chart inside your cluster

# Run with kubernetes

## Install alitwitter chart
```
helm install alitwitterchart ./
```

## Access on chrome
```
<minikube_ip>:30000/tweets
```