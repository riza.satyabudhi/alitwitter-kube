#!/bin/sh
set -e
rm -f /alitwitter/tmp/pids/server.pid
bundle exec rake assets:precompile
exec "$@"
